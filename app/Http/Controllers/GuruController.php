<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\GuruModel;
use Illuminate\Support\Facades\DB;

class GuruController extends Controller
{

    public function __construct()
    {
        $this->GuruModel = new GuruModel();
        $this->middleware('auth');
    }
    //index
    public function index()
    {
        //$data = [
        //    'guru' => $this->GuruModel->allData(),
        //];

        $data['guru'] = DB::table('tbl_guru')->orderBy('id_guru', 'desc')->get();

        ($data);
        return view('v_guru', $data);
    }
    //detail
    public function detail($id_guru)
    {
        if (!$this->GuruModel->detailData($id_guru)) {
            abort(404);
        }
        $data = [
            'guru' => $this->GuruModel->detailData($id_guru),
        ];

        ($data);
        return view('v_detailguru', $data);
    }

    //add
    public function add()
    {
        return view('v_addguru');
    }

    //insert
    public function insert()
    {


        Request()->validate([
            'nip' => 'required|unique:tbl_guru,nip|min:4 | max:5',
            'nama_guru' => 'required',
            'mapel' => 'required',
            'foto_guru' => 'required|mimes:jpg,jpeg,bmp,png|max:5000',
        ], [
            'nip.required' => 'Harus Diisi !!',
            'nip.unique' => 'Nip Ini Sudah Ada !!',
            'nip.min' => 'Minimal 4 karakter !!',
            'nip.max' => 'Maximal 5 Karakter !!',
            'nama_guru.required' => 'Harus Diisi !!',
            'mapel.required' => 'Harus Diisi !!',
            'foto_guru.required' => 'Harus Diisi !!',

        ]);

        //jika validasi tidak ada maka lakukan simpan data
        //upload gambar/foto
        $file = Request()->foto_guru;
        $fileName = Request()->nip . '.' . $file->extension();
        $file->move(public_path('image_guru'), $fileName);

        $data = [
            'nip' => Request()->nip,
            'nama_guru' => Request()->nama_guru,
            'mapel' => Request()->mapel,
            'Foto_guru' => $fileName,

        ];

        $this->GuruModel->addData($data);
        return redirect()->route('guru')->with('pesan', 'Data Berhasil Di Tambahkan !!!');
    }

    //edit
    public function edit($id_guru)
    {
        if (!$this->GuruModel->detailData($id_guru)) {
            abort(404);
        }
        $data = [
            'guru' => $this->GuruModel->detailData($id_guru),
        ];
        return view('v_editguru', $data);
    }

    //update
    public function update($id_guru)
    {


        Request()->validate([
            'nip' => 'required|min:4 | max:5',
            'nama_guru' => 'required',
            'mapel' => 'required',
            'foto_guru' => 'mimes:jpg,jpeg,bmp,png|max:5000',
        ], [
            'nip.required' => 'Harus Diisi !!',
            'nip.min' => 'Minimal 4 karakter !!',
            'nip.max' => 'Maximal 5 Karakter !!',
            'nama_guru.required' => 'Harus Diisi !!',
            'mapel.required' => 'Harus Diisi !!',
        ]);

        //jika validasi tidak ada maka lakukan simpan data
        if (Request()->foto_guru <> "") {
            //jika ingin ganti foto
            //upload gambar/foto
            $file = Request()->foto_guru;
            $fileName = Request()->nip . '.' . $file->extension();
            $file->move(public_path('image_guru'), $fileName);
            $data = [
                'nip' => Request()->nip,
                'nama_guru' => Request()->nama_guru,
                'mapel' => Request()->mapel,
                'Foto_guru' => $fileName,
            ];
            $this->GuruModel->editData($id_guru, $data);
        } else {
            //jika tidak ingin ganti foto
            $data = [
                'nip' => Request()->nip,
                'nama_guru' => Request()->nama_guru,
                'mapel' => Request()->mapel,
            ];
            $this->GuruModel->editData($id_guru, $data);
        }

        return redirect()->route('guru')->with('pesan', 'Data Berhasil Di Update !!!');
    }

    public function delete($id_guru)
    {
        //hapus atau delete foto
        $guru = $this->GuruModel->detailData($id_guru);
        if ($guru->foto_guru <> "") {
            unlink(public_path('image_guru') . '/' . $guru->foto_guru);
        }
        $this->GuruModel->deleteData($id_guru);
        return redirect()->route('guru')->with('pesan', 'Data Berhasil Di Delete !');
    }
}