@extends('layout.v_template')
@section('title', 'Siswa')

@section('content')
    <a href="{{ url('/siswa/print') }}" target="_blank" class="btn btn-primary">Print To Printer</a>
    <a href="{{ url('/siswa/printpdf') }}" target="_blank" class="btn btn-success">Print To PDF</a>


    <table class="table table-bordered">
        <thead>
            <tr>
                <th>No</th>
                <th>NIS</th>
                <th>Nama Siswa</th>
                <th>Kelas</th>
                <th>Mapel</th>
            </tr>
        </thead>
        <tbody>
            <?php $no = 1; ?>
            @foreach ($siswa as $data)
                <tr>
                    <td>{{ $no++ }}</td>
                    <td>{{ $data->nis }}</td>
                    <td>{{ $data->nama_siswa }}</td>
                    <td>{{ $data->kelas }}</td>
                    <td>{{ $data->mapel }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

@endsection
